VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "Sheet7"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = True
Option Explicit

Private Sub Worksheet_Change(ByVal Target As Range)
    Dim c As Integer, _
        sWorkDate As String, sGroupName As String, _
        aSheet As Worksheet, _
        loTbl As Excel.ListObject, _
        loRow As Range
        'r As Long,

    Set aSheet = ActiveSheet

    Application.EnableEvents = False
    ThisWorkbook.EventsOn = False
    UnprotectSheet aSheet.Name
' If the changed cell is the Work Date, Filter the Group Name list
    If Not Intersect(Target, ActiveSheet.Range("GROUP_WORK_DATE")) Is Nothing Then
        'Application.EnableEvents = False
        If IsEmpty(Target.Value) Then
' Reset the Group Name filter when Work Date is blank
            With Sheets("DATA_HISTORY").ListObjects("GROUP_PAYOUTS_TOTALS")
                For c = 1 To .Range.Columns.Count
                    .Range.AutoFilter Field:=c
                Next
            End With
' Without a Date there is no Data. - Unfortunately ClearAllData creates infinite recursion
            With aSheet
                .Range("GROUP_PEOPLE").ClearContents
                .Range("GROUP_WORK_LOSS").ClearContents
                .Range("GROUP_HOURS").ClearContents
                '.Range("GROUP_PAY_TOTALS").ClearContents
                .Range("GROUP_PIECES_LIST").ClearContents
                .Range("GROUP_PIECES_COUNTS").ClearContents
                '.Range("GROUP_GROUP_PAYOUTS").ClearContents
                '.Range("GROUP_PIECES_SUPERVISOR_PAYOUTS").ClearContents
                .Range("GROUP_SUPERVISORS").ClearContents
                '.Range("GROUP_SUPERVISOR_PAYOUTS").ClearContents
            End With
        Else
' Filter the Group Names when Work Date has a value
            Sheets("DATA_UTILITIES").Visible = xlSheetVisible
            UnprotectSheet "DATA_UTILITIES"
            Application.Goto Reference:="UNIQUE_GROUP_NAMES"
            Selection.ClearContents
            Selection.End(xlUp).Select
            Selection.Offset(1, 0).Select
            With Sheets("DATA_HISTORY")
                With .ListObjects("GROUP_PAYOUTS_TOTALS")
                    .AutoFilter.ShowAllData
                    .Range.AutoFilter _
                        Field:=1, Operator:=xlFilterValues, Criteria1:=Target.Value
                End With
                .UsedRange.SpecialCells(xlCellTypeVisible) _
                .Range("GROUP_PAYOUTS_TOTALS[Group Sheet Name]").Copy
            End With
            Selection.PasteSpecial Paste:=xlValues
            aSheet.Activate
            ProtectSheet "DATA_UTILITIES"
            Sheets("DATA_UTILITIES").Visible = xlSheetVeryHidden
            Application.CutCopyMode = False
        End If
'        Application.EnableEvents = True
' If the changed cell is the Group Name, check Work Date for a value & fill in data
    ElseIf Not Intersect(Target, ActiveSheet.Range("GROUP_NAME")) Is Nothing Then
        If Target.Text = "" Then
' Without a Group there is no Data. - Unfortunately ClearAllData creates infinite recursion
            aSheet.Range("GROUP_WORK_DATE").ClearContents
        ElseIf aSheet.Range("GROUP_WORK_DATE").Text = "" Then
' No dice, Jack.
            'MsgBox "Need to select a Work Date first."
            'aSheet.Range("GROUP_WORK_DATE").Select
        Else
' Run the data fill-in
            sWorkDate = [GROUP_WORK_DATE].Value
            sGroupName = [GROUP_NAME].Value
            ' Clear data from previous lookup, which also clears the Date & Group
            ClearAllLookupData
            ' Reset the Date & Group values
            [GROUP_WORK_DATE].Value = sWorkDate
            [GROUP_NAME].Value = sGroupName
' Go through the relevant tables, filtering on the date/group columns
' Once they're filtered, copy their data back to the lookup sheet.
            With Sheets("DATA_HISTORY")
'''' This block is not needed - the block totals are still calculated, only the line items are static
'                With .ListObjects("GROUP_PAYOUTS_TOTALS")
'                    .AutoFilter.ShowAllData
'                    .Range.AutoFilter Field:=1, Criteria1:=sWorkDate
'                    .Range.AutoFilter Field:=2, Criteria1:=sGroupName
'                    'aSheet.Range("GROUP_TOTAL_GROUP_PAYOUT") = .ListColumns("Labor Total").DataBodyRange(1).Value
'                    'aSheet.Range("GROUP_TOTAL_GROUP_PAY") = .ListColumns("Actual Payout Total").DataBodyRange(1).Value
'                    .AutoFilter.ShowAllData
'                End With
''''
                Set loTbl = .ListObjects("GROUP_PAYOUTS_PEOPLE")
                With loTbl
                    .AutoFilter.ShowAllData
                    .Range.AutoFilter Field:=1, Criteria1:=sWorkDate
                    .Range.AutoFilter Field:=2, Criteria1:=sGroupName
                    aSheet.Range("B6").Activate
                    c = -1
                    For Each loRow In loTbl.DataBodyRange.SpecialCells(xlCellTypeVisible).Rows
                        ' Get the team lead - the one with Hours entered
                        ' If we're going to allow multiple Roles, this IF may be bad logic:
                        'If Intersect(loRow.EntireRow, loTbl.ListColumns("Role").Range).Text = "LEAD" Then
                        '    ActiveCell.Offset(0, -1) = Intersect(loRow.EntireRow, loTbl.ListColumns("Role").Range).Value
                        '    ActiveCell.Offset(0, 0) = Intersect(loRow.EntireRow, loTbl.ListColumns("Person").Range).Value
                        '    ActiveCell.Offset(0, 1) = Intersect(loRow.EntireRow, loTbl.ListColumns("Deductions").Range).Value
                        '    ActiveCell.Offset(0, 2) = Intersect(loRow.EntireRow, loTbl.ListColumns("Hours").Range).Value
                        '    ActiveCell.Offset(0, 3) = Intersect(loRow.EntireRow, loTbl.ListColumns("Person Payout").Range).Value
                        'Else
                        ' Perhaps better just to fill in every row regardless what data is in it...
                            c = c + 1
                            ActiveCell.Offset(c, -1) = Intersect(loRow.EntireRow, loTbl.ListColumns("Role").Range).Value
                            ActiveCell.Offset(c, 0) = Intersect(loRow.EntireRow, loTbl.ListColumns("Person").Range).Value
                            ActiveCell.Offset(c, 1) = Intersect(loRow.EntireRow, loTbl.ListColumns("Deductions").Range).Value
                            ActiveCell.Offset(c, 2) = Intersect(loRow.EntireRow, loTbl.ListColumns("Hours").Range).Value
                            'ActiveCell.Offset(c, 3) = Intersect(loRow.EntireRow, loTbl.ListColumns("Person Payout").Range).Value
                        'End If
                    Next
                    .AutoFilter.ShowAllData
                End With
                
                Set loTbl = .ListObjects("GROUP_PAYOUTS_PIECES")
                With loTbl
                    .AutoFilter.ShowAllData
                    .Range.AutoFilter Field:=1, Criteria1:=sWorkDate
                    .Range.AutoFilter Field:=2, Criteria1:=sGroupName
                    aSheet.Range("B25").Activate
                    c = -1
                    For Each loRow In loTbl.DataBodyRange.SpecialCells(xlCellTypeVisible).Rows
                        c = c + 1
                        ActiveCell.Offset(c, 0) = Intersect(loRow.EntireRow, loTbl.ListColumns("Piece Type").Range).Value
                        ActiveCell.Offset(c, 1) = Intersect(loRow.EntireRow, loTbl.ListColumns("Piece Total").Range).Value
                        'ActiveCell.Offset(c, 2) = Intersect(loRow.EntireRow, loTbl.ListColumns("Group Payout").Range).Value
                        'ActiveCell.Offset(c, 3) = Intersect(loRow.EntireRow, loTbl.ListColumns("Supervisor Incentive").Range).Value
                    Next
                    .AutoFilter.ShowAllData
                End With
            
                Set loTbl = .ListObjects("GROUP_PAYOUTS_SUPERVISORS")
                With loTbl
                    .AutoFilter.ShowAllData
                    .Range.AutoFilter Field:=1, Criteria1:=sWorkDate
                    .Range.AutoFilter Field:=2, Criteria1:=sGroupName
                    aSheet.Range("B36").Activate
                    c = -1
                    For Each loRow In loTbl.DataBodyRange.SpecialCells(xlCellTypeVisible).Rows
                        c = c + 1
                        ActiveCell.Offset(c, 0) = Intersect(loRow.EntireRow, loTbl.ListColumns("Supervisor Name").Range).Value
                        'ActiveCell.Offset(c, 1) = Intersect(loRow.EntireRow, loTbl.ListColumns("Incentive Total").Range).Value
                    Next
                    .AutoFilter.ShowAllData
                End With
            End With
        End If
    End If
    
    ProtectSheet aSheet.Name
    ThisWorkbook.EventsOn = True
    Application.EnableEvents = True
End Sub

