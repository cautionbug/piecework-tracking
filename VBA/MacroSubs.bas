Attribute VB_Name = "MacroSubs"
Option Explicit

Sub AddGroupSheet()
    Dim newSheet As Worksheet, newName As String
    
    With Sheets("Group")
        .Visible = xlSheetVisible
        .Copy Before:=Sheets("Tear Down")
        ProtectSheet .Name
        .Visible = xlSheetVeryHidden
    End With
    Set newSheet = Sheets(Sheets("Tear Down").Index - 1)
    Do
        newName = InputBox("Enter the name of your new group:", "Give new sheet a UNIQUE name", "Group X")
    Loop Until newName = "" Or SheetExists(newName) = False
    
    If newName = "" Then
        Application.DisplayAlerts = False
        newSheet.Delete
        Application.DisplayAlerts = True
        Sheets("CONTROLS").Activate
    Else
        newSheet.Name = newName
    End If

End Sub

Sub AddRemovePeopleRows()
    AddRemoveRows "GROUP_PEOPLE", 15
End Sub

Sub AddRemovePieceTypeRows()
    AddRemoveRows "GROUP_PIECES_LIST", 8
End Sub

Sub AddRemoveSupervisorRows()
    AddRemoveRows "GROUP_SUPERVISORS", 4
End Sub

Sub ClearAllData()
    With ActiveSheet
        .Range("GROUP_PEOPLE").ClearContents
        .Range("GROUP_ROLES").ClearContents
        .Range("GROUP_SUPERVISORS").ClearContents
    End With
    ClearDailyData
End Sub

Sub ClearAllLookupData()
    If ThisWorkbook.EventsOn = True Then
        Application.EnableEvents = False
        UnprotectSheet ActiveSheet.Name
    End If
    Application.Goto "'" & ActiveSheet.Name & "'!GROUP_NAME"
    Selection.ClearContents
    ClearAllData
    If ThisWorkbook.EventsOn = True Then
        ProtectSheet ActiveSheet.Name
        Application.EnableEvents = ThisWorkbook.EventsOn
    End If
End Sub

Sub ClearDailyData()
    With ActiveSheet
        .Range("GROUP_WORK_DATE").ClearContents
        .Range("GROUP_WORK_LOSS").ClearContents
        .Range("GROUP_HOURS").ClearContents
        .Range("GROUP_PIECES_LIST").ClearContents
        .Range("GROUP_PIECES_COUNTS").ClearContents
        .Range("DT_MECHANICAL_HOURS").ClearContents
        .Range("DT_WEATHER_HOURS").ClearContents
        .Range("DT_PERSONNEL_HOURS").ClearContents
    End With
End Sub

Sub SaveGroupData()
    Dim shHistory As Worksheet, aSheet As Worksheet, _
        loTable As ListObject, _
        lrInsert As ListRow, r As Range, _
        sWorkDate As String, sGroupName As String, sRole As String, sPerson As String, _
        sPieceType As String, sSupervisor As String, _
        iHours As Integer, iPiecesLoaded As Integer, iVisible As Integer, _
        dDeduction As Double, dHourlyRate As Double, dPayTotal As Double, _
        dPieceRate As Double, dPieceTypeSupervisorRate As Double, dSupervisorPayout As Double
        'colDate As Long, colGroup As Long, _

    Set shHistory = Sheets("DATA_HISTORY")
    Set aSheet = ActiveSheet
    sWorkDate = aSheet.[GROUP_WORK_DATE]
    sGroupName = aSheet.Name
    
    If sWorkDate = "" Then
        MsgBox "Work Date is required."
        aSheet.[GROUP_WORK_DATE].Select
        Exit Sub
    End If
    
' Insert the HISTORY_DATES record (if it doesn't exist yet - multiple groups on the same date)
    Set loTable = shHistory.ListObjects("HISTORY_DATES")
    ' VBA doesn't short-circuit evaluate.
    ' Since the evaluations are on different levels, if the first is false,
    '   we can't evaluate the second without an error.
    ' This bypasses that.
    Select Case True
    Case (loTable.DataBodyRange Is Nothing), _
        (loTable.DataBodyRange.Find(What:=sWorkDate) Is Nothing)
        Set lrInsert = loTable.ListRows.Add
        lrInsert.Range.Columns(1) = sWorkDate
        With loTable
            .Sort.SortFields.Add Key:=.DataBodyRange.Columns(1), SortOn:=xlSortOnValues, _
                Order:=xlAscending, DataOption:=xlSortNormal
            With .Sort
                .Header = xlYes
                .MatchCase = False
                .Orientation = xlTopToBottom
                .SortMethod = xlPinYin
                .Apply
            End With
        End With
    End Select
' Insert the GROUP_PAYOUTS_TOTALS record
    Set loTable = shHistory.ListObjects("GROUP_PAYOUTS_TOTALS")
    Set lrInsert = loTable.ListRows.Add
    With lrInsert.Range
        .Columns(1) = sWorkDate
        .Columns(2) = sGroupName
        .Columns(3) = aSheet.[GROUP_TOTAL_GROUP_PAYOUT]
        .Columns(4) = aSheet.[GROUP_TOTAL_GROUP_PAYOUT] * aSheet.[GROUP_TOTAL_WORK_LOSS]
    End With
    
' Insert GROUP_PAYOUTS_PEOPLE records
    Set loTable = shHistory.ListObjects("GROUP_PAYOUTS_PEOPLE")
    With aSheet
        For Each r In .[GROUP_PEOPLE]
            If Not IsEmpty(r) Then
                ' Collect the record values
                sRole = r.Offset(0, -1)
                sPerson = r
                dDeduction = r.Offset(0, 1)
                iHours = r.Offset(0, 2)
                ' dHourlyRate = IfNotNamedRange("CORE_OTHER_RATE_" & sRole, 0)
                If sRole = "" Then
                    dHourlyRate = 0
                Else
                    dHourlyRate = WorksheetFunction.VLookup(sRole, [CORE_OTHER_RATES], 2, False)
                End If
                dPayTotal = r.Offset(0, 3)
                ' Insert them into the table
                Set lrInsert = loTable.ListRows.Add
                With lrInsert.Range
                    .Columns(1) = sWorkDate
                    .Columns(2) = sGroupName
                    .Columns(3) = sRole
                    .Columns(4) = sPerson
                    .Columns(5) = dDeduction
                    .Columns(6) = iHours
                    .Columns(7) = dHourlyRate
                    .Columns(8) = dPayTotal
                End With
            End If
        Next
    'End With
' Insert GROUP_PAYOUTS_PIECES records
    Set loTable = shHistory.ListObjects("GROUP_PAYOUTS_PIECES")
    'With aSheet
        For Each r In .[GROUP_PIECES_LIST]
            If Not IsEmpty(r) Then
                ' Collect the record values
                sPieceType = r
                iPiecesLoaded = r.Offset(0, 1)
                dPieceRate = WorksheetFunction.VLookup(r, [CORE_PIECE_TYPES], 2, False)
                dPieceTypeSupervisorRate = WorksheetFunction.VLookup(r, [CORE_PIECE_TYPES], 4, False)
                ' Insert them into the table
                Set lrInsert = loTable.ListRows.Add
                With lrInsert.Range
                    .Columns(1) = sWorkDate
                    .Columns(2) = sGroupName
                    .Columns(3) = sPieceType
                    .Columns(4) = iPiecesLoaded
                    .Columns(5) = dPieceRate
                    ' Column 6 is calculated
                    .Columns(7) = dPieceTypeSupervisorRate
                    ' Column 8 is calculated
                End With
            End If
        Next
    'End With
' Insert GROUP_PAYOUTS_SUPERVISORS records
    Set loTable = shHistory.ListObjects("GROUP_PAYOUTS_SUPERVISORS")
    'With aSheet
        For Each r In .[GROUP_SUPERVISORS]
            If Not IsEmpty(r) Then
                ' Collect the record values
                sSupervisor = r
                dSupervisorPayout = r.Offset(0, 1)
                ' Insert them into the table
                Set lrInsert = loTable.ListRows.Add
                With lrInsert.Range
                    .Columns(1) = sWorkDate
                    .Columns(2) = sGroupName
                    .Columns(3) = sSupervisor
                    .Columns(4) = dSupervisorPayout
                End With
            End If
        Next
    End With
    
    ' Clear the Daily Data to prevent re-save & data duplication
    ClearDailyData
' Rebuild the Unique Group Names list on DATA_UTILTIES
    
    With Sheets("DATA_UTILITIES")
        .Visible = xlSheetVisible
        UnprotectSheet .Name
        '.Activate
        .[UNIQUE_GROUP_NAMES].ClearContents
        With Sheets("DATA_HISTORY")
            iVisible = .Visible
            .Visible = xlSheetVisible
            .Activate
            .Range("GROUP_PAYOUTS_TOTALS[Group Sheet Name]").Select
            Selection.Copy
            .Visible = iVisible
        End With
        .Activate
        .[UNIQUE_GROUP_NAMES].Select
        Selection.PasteSpecial Paste:=xlPasteValues
        Application.CutCopyMode = False
        Selection.RemoveDuplicates Columns:=1, Header:=xlNo
        Range(Selection.End(xlUp), Selection.End(xlDown)).Sort Key1:=Selection, Order1:=xlAscending, Header:=xlYes
        aSheet.Activate
        ProtectSheet .Name
        .Visible = xlSheetVeryHidden
    End With
End Sub

Sub SaveTeardownData()
    Dim r As Range, lrInsert As ListRow
    
    If ActiveSheet.[TEARDOWN_DATE] = "" Then
        MsgBox "Work Date is required."
        ActiveSheet.[TEARDOWN_DATE].Select
        Exit Sub
    End If
    
    For Each r In ActiveSheet.[TEARDOWN_PEOPLE]
        If Not IsEmpty(r) Then
            Set lrInsert = Sheets("DATA_HISTORY").ListObjects("TEARDOWN").ListRows.Add
            With lrInsert.Range
                .Columns(1) = ActiveSheet.[TEARDOWN_DATE]
                .Columns(2) = r
                .Columns(3) = r.Offset(0, 1)
                .Columns(4) = r.Offset(0, 2)
                .Columns(5) = r.Offset(0, 3)
            End With
        End If
    Next
End Sub

Sub ClearTeardownData()
    With ActiveSheet
        .Range("TEARDOWN_DATE").ClearContents
        .Range("TEARDOWN_PEOPLE").ClearContents
        .Range("TEARDOWN_PIECE_COUNT").ClearContents
        .Range("TEARDOWN_PIECE_TYPE").ClearContents
    End With
End Sub

Sub ShowHistorySheet()
    With Sheets("DATA_HISTORY")
        .Visible = xlSheetVisible
        .Activate
    End With
End Sub

Sub HideHistorySheet()
    Sheets("DATA_HISTORY").Visible = xlSheetHidden
End Sub

Sub ShowDataCoreSheet()
    With Sheets("DATA_CORE")
        .Visible = xlSheetVisible
        .Activate
    End With
End Sub

Sub HideDataCoreSheet()
    Sheets("DATA_CORE").Visible = xlSheetHidden
End Sub

Sub ClearHistorySheet()
    Dim loTable As ListObject
    If MsgBox("Are you CERTAIN you want to do this?" & vbNewLine & _
            "You will lose all existing data in the History sheet." & vbNewLine & _
            "Make SURE you have copied the sheet out to a new workbook first." & vbNewLine & vbNewLine & _
            "Continue?", vbYesNo) = vbYes Then
        With Sheets("DATA_HISTORY")
            .Visible = xlSheetVisible
            .Activate
            For Each loTable In .ListObjects
                If Not loTable.DataBodyRange Is Nothing Then
                    loTable.AutoFilter.ShowAllData
                    loTable.DataBodyRange.Rows.Delete
                End If
            Next
            .Visible = xlSheetHidden
        End With
        With Sheets("DATA_UTILITIES")
            UnprotectSheet .Name
            .[UNIQUE_GROUP_NAMES].ClearContents
            ProtectSheet .Name
        End With
        Sheets("CONTROLS").Activate
    End If
End Sub

Sub OpenHelpForm()
    Load frmHelp
    frmHelp.Show
End Sub

