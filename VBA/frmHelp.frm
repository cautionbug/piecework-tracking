VERSION 5.00
Begin {C62A69F0-16DC-11CE-9E98-00AA00574A4F} frmHelp 
   Caption         =   "Help & Support"
   ClientHeight    =   5325
   ClientLeft      =   45
   ClientTop       =   375
   ClientWidth     =   8670
   OleObjectBlob   =   "frmHelp.frx":0000
   StartUpPosition =   1  'CenterOwner
End
Attribute VB_Name = "frmHelp"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub cmdClose_Click()
    Unload Me
End Sub

Private Sub Label5_Click()
    Dim Link As String
    Link = "http://goo.gl/1jOoGs"
    On Error GoTo ErrHandler
    
    ActiveWorkbook.FollowHyperlink Address:=Link, NewWindow:=True
    Unload Me
    Exit Sub
ErrHandler:
    MsgBox "Can't open " & Link
End Sub

'---------------------------------------------------------------------------------------
' Procedure : UserForm_Initialize
' Author    : Joe Theuerkauf
' Date      : 2015-02-08
' Purpose   : Set the Utility's version number and center the form.
'---------------------------------------------------------------------------------------
'
Private Sub UserForm_Initialize()
    Dim dLeft As Double, dTop As Double
    
    lblVersionNumber.Caption = VERSION_NUMBER & " (" & VERSION_DATE & ")"
    dLeft = (Application.Width - Me.Width) / 2
    dTop = (Application.Height - Me.Height) / 2
    Me.Top = dTop
    Me.Left = dLeft
End Sub
 
'---------------------------------------------------------------------------------------
' Procedure : lblEmail_Click
' Author    : Joe Theuerkauf
' Date      : 2015-01-18
' Purpose   :
'---------------------------------------------------------------------------------------
'
'Private Sub lblEmail_Click()
'On Error GoTo ErrHandler
'    Dim sURL As String
'
'    Application.DisplayAlerts = False
'    sURL = "mailto:" & Me.lblEmail.Caption & "?subject=Help%20request,%20Piece%20Rate%20Tracking%20Utility"
'    ActiveWorkbook.FollowHyperlink Address:=sURL
'ErrHandler:
'    If Err.Number <> 0 Then
'        Application.DisplayAlerts = True
'        MsgBox "Error " & CStr(Err.Number) & ": " & Err.Description
'    Else
'        Application.DisplayAlerts = True
'    End If
'End Sub

