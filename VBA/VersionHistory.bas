Attribute VB_Name = "VersionHistory"
Option Explicit

Public Const VERSION_DATE = "2015-02-16"
Public Const VERSION_NUMBER = "1.3.4"

' Version 1.3.4 (2015-02-16
' * Fixed bug with empty "People" ranges
'
' Version 1.3.3 (2015-02-02)
' * Fixed Data Validation message
' * Added editable cells & named ranges for DownTime - no integration in data yet.
' * Make Group sheet always protected after new group sheet is generated
' * Added DownTime cells to ClearDailyData
'
' Version 1.3.2 (2015-01-25)
' * Changed how Group sheet is created (allows Cancel or empty value to remove new sheet)
' * DATA_CORE copies Supervisors & People to CORE_ALL_PEOPLE range on DATA_UTILITIES
'
' Version 1.3.1 (2015-01-18)
' Packaging up for sale:
' * Clearing test/demo data
' * Added Support form
'
' Version: 1.3.0 (2015-01-11)
' Requests from Garry for additional development & possible broader sales:
' * Make more generic
'   > Change labeling, named ranges, etc. away from "Rack" specific identification
'   > Recheck macros & VBA code to make sure it's working
'   > Recheck data validation & named ranges
'   + Add clear-data macro for Teardown sheet
'
' Version: 1.2.0 (2013-04-04)
' Zoho Ticket 288:
' * Changed People section to allow more flexible use of "LEAD":
'   + multiple allowed
'   + conditional formatting added
'   + GROUP_HOURS range expanded
'   + line total formula adjusted for optional usage
'   - NOTE: Cells cannot be conditionally locked, so the cells are unlocked.
'       If a value is entered but NOT the role in col A, the Hours will NOT be calculated.
'
' Version: 1.1.0 (2013-04-04)
' * Added this History module
' * Added latest version to Comment property of file
' Zoho Ticket 288:
' * Added Function AddRemoveRows in ExtraMethod module
' * Added Subs AddRemovePeopleRows, AddRemoveRackRows, AddRemoveSupervisorRows in MacroSubs module
' * Added buttons corresponding to above macro Subs to Group sheet
'
