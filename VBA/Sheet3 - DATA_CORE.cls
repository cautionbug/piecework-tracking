VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "Sheet3"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = True
Private Sub Worksheet_Deactivate()
    Dim actSheet As Worksheet, aSheet As Worksheet, bSheet As Worksheet, _
        hasPeople As Boolean, hasSupervisors As Boolean
    
    Set actSheet = ActiveSheet
    Set aSheet = Sheets("DATA_CORE")
    Set bSheet = Sheets("DATA_UTILITIES")
    
    ThisWorkbook.EventsOn = False
    Application.EnableEvents = False
    
    With aSheet
        If IsEmpty(.[CORE_PEOPLE].Value2) = False Then _
            hasPeople = True
        If IsEmpty(.[CORE_SUPERVISORS].Value2) = False Then _
            hasSupervisors = True
        
        If hasPeople = True Or hasSupervisors = True Then
            With bSheet
                bSheet.Visible = xlSheetVisible
                UnprotectSheet .Name
                .Activate
                .[CORE_ALL_PEOPLE].ClearContents
            End With
            
            .Activate
            If hasSupervisors = True Then
                .[CORE_SUPERVISORS].Select
                Selection.Sort Key1:=Selection, Order1:=xlAscending, Header:=xlNo
                Selection.Copy
                With bSheet
                    .Activate
                    .[CORE_ALL_PEOPLE].Select
                    Selection.PasteSpecial Paste:=xlPasteValues
                End With
                .Activate
            End If
            If hasPeople = True Then
                .[CORE_PEOPLE].Select
                Selection.Sort Key1:=Selection, Order1:=xlAscending, Header:=xlNo
                Selection.Copy
                With bSheet
                    .Activate
                    .[CORE_ALL_PEOPLE].Select
                    If IsEmpty(.[CORE_ALL_PEOPLE].Value2) = False Then _
                        ActiveCell.End(xlDown).Offset(1, 0).Select
                    Selection.PasteSpecial Paste:=xlPasteValues
                End With
            End If
            
            Application.CutCopyMode = False
            
            With bSheet
                ProtectSheet .Name
                .Visible = xlSheetVeryHidden
            End With
        End If
    End With
    
    actSheet.Activate
    
    Application.EnableEvents = True
    ThisWorkbook.EventsOn = True
End Sub
