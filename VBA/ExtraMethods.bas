Attribute VB_Name = "ExtraMethods"
Option Explicit

Public Const SHEET_PASSWORD = "@Thook!PW$"

'---------------------------------------------------------------------------------------
' Procedure : SheetExists
' Author    : Joe Theuerkauf
' Date      : 2013-02-27
' Purpose   : When making a copy of "Group" sheet, checks the entered name isn't
'   already an existing sheet.
'---------------------------------------------------------------------------------------
'
Function SheetExists(sheetName As String) As Boolean
    Dim x As Object
    On Error Resume Next
    Set x = ActiveWorkbook.Sheets(sheetName)
    If Err = 0 Then SheetExists = True _
        Else: SheetExists = False
End Function

'---------------------------------------------------------------------------------------
' Procedure : IfNotNamedRange
' Author    : Joe Theuerkauf
' Date      : 2013-03-02
' Purpose   : If rangeName is not a Named Range, use the substitute value
'---------------------------------------------------------------------------------------
'
Function IfNotNamedRange(rangeName As String, altValue)
    On Error Resume Next
    IfNotNamedRange = altValue
    IfNotNamedRange = IIf(Len(Names(rangeName).Name) <> 0, Range(rangeName), altValue)
End Function

'---------------------------------------------------------------------------------------
' Procedure : CellComment
' Author    : Joe Theuerkauf
' Date      : 2013-03-02
' Purpose   : Access the cell comment from a sheet formula
'---------------------------------------------------------------------------------------
'
Function CellComment(inCell As Range) As String
    CellComment = inCell.Comment.Text
End Function


'---------------------------------------------------------------------------------------
' Procedure : AddRemoveRows
' Author    : Joe Theuerkauf
' Date      : 2013-04-04
' Purpose   : Add/Remove rows to list ranges on the active Group worksheet
'---------------------------------------------------------------------------------------
'
Function AddRemoveRows(rangeName As String, minRows As Long)
    Dim rowCount As Long, rowInput As String
    
    rowCount = 1
    Do
    On Error GoTo rowCountError
rowCountError:
        rowInput = InputBox("Enter the number of rows, integers only:" & vbNewLine & _
            "Positive number to ADD rows" & vbNewLine & "Negative number to REMOVE rows.", "Add/Remove Rows", rowCount)
        If rowInput = "" Then Exit Function
    Loop Until IsNumeric(rowInput) 'And (rowCount - Int(rowCount) = 0)
    rowCount = CLng(rowInput)
    
    With ActiveSheet
        UnprotectSheet .Name
        .Range(rangeName).Cells(.Range(rangeName).Rows.Count, 1).EntireRow.Select
        If rowCount < 0 Then ' DELETE ROWS
            Do While rowCount <> 0 And .Range(rangeName).Rows.Count > minRows
                Selection.Offset(-1, 0).EntireRow.Select
                Selection.Delete Shift:=xlUp
                rowCount = rowCount + 1
            Loop
        Else ' INSERT ROWS
            Do While rowCount <> 0
                Selection.Insert Shift:=xlDown, CopyOrigin:=xlFormatFromLeftOrAbove
                rowCount = rowCount - 1
            Loop
        End If
        ProtectSheet .Name
    End With
End Function

'---------------------------------------------------------------------------------------
' Procedure : ProtectSheet
' Author    : Joe Theuerkauf
' Date      : 2015-01-12
' Purpose   : Consistently Protect Worksheet with same settings
'---------------------------------------------------------------------------------------
'
Function ProtectSheet(sheetName As String)
    Sheets(sheetName).Protect DrawingObjects:=True, Contents:=True, Scenarios:=True, Password:=SHEET_PASSWORD
End Function

'---------------------------------------------------------------------------------------
' Procedure : UnprotectSheet
' Author    : Joe Theuerkauf
' Date      : 2015-01-12
' Purpose   : Consistently Unprotect Worksheet
'---------------------------------------------------------------------------------------
'
Function UnprotectSheet(sheetName As String)
    Sheets(sheetName).Unprotect Password:=SHEET_PASSWORD
End Function

' For a later version
'---------------------------------------------------------------------------------------
' Procedure : GetPathAndFile
' Author    : Joe Theuerkauf
' Date      : 2013-02-27
' Purpose   : Open a FileDialog & ask user to select the path to a file.
'   Return the path & file for things like setting the 7zip.exe location, the archive zip target, etc.
'http://stackoverflow.com/questions/12687536/vba-how-to-get-selected-path-and-name-of-the-file-using-open-file-dialog-contr
'---------------------------------------------------------------------------------------
'
'Function GetPathAndFile(sTitle As String, rIFNRef As Range) As String
'    Dim lngCount As Long, _
'        cl As Range, _
'        fullPath As String
'
'    'Set cl = ActiveCell
'    ' Open the file dialog
'    With Application.FileDialog(msoFileDialogOpen)
'        .Title = sTitle
'        .InitialFileName = Range(rIFNRef).Value
'        .AllowMultiSelect = True
'        .Show
'        ' Display paths of each file selected
'        For lngCount = 1 To .SelectedItems.Count
'            fullPath = fullPath & .SelectedItems(lngCount)
'            ' Add Hyperlinks
'            'cl.Worksheet.Hyperlinks.Add _
'                Anchor:=cl, Address:=.SelectedItems(lngCount), _
'                TextToDisplay:=.SelectedItems(lngCount)
'            ' Add file name
'            'cl.Offset(0, 1) = _
'            '    Mid(.SelectedItems(lngCount), InStrRev(.SelectedItems(lngCount), "\") + 1)
'            ' Add file as formula
'            'cl.Offset(0, 1).FormulaR1C1 = _
'                 "=TRIM(RIGHT(SUBSTITUTE(RC[-1],""\"",REPT("" "",99)),99))"
'            'Set cl = cl.Offset(1, 0)
'        Next lngCount
'    End With
'End Function

